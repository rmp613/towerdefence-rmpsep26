﻿using UnityEngine;
using System.Collections;

public class AssaultRifle : BaseWeapon {

	GameObject ch;

	bool isDown;

	public AudioClip assaultRifleReload;

	float timeBetweenShots = 0;

	// Use this for initialization
	void Start ()
	{
		gunAnim = GetComponent<Animation> ();
		gunAnim.Play ("Draw");
		ch = GameObject.FindGameObjectWithTag ("Crosshair");
		ch.SetActive (true);
		gunSound = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		UpdateAmmo ();

		Debug.Log (currentAmmo + "/" + currentExtraAmmo);


		isDown = Input.GetMouseButton (0);

		if (isDown) {
			timeBetweenShots += Time.deltaTime;
			if (timeBetweenShots > 0.1f){
				gunAnim.Stop ();
				timeBetweenShots = 0;
			}
		}

		if (!gunAnim.isPlaying) {


			if (Input.GetButtonDown ("Fire2")) {
				if (isADS) {
					gunAnim.Play ("UnADS");
					isADS = false;
				} else {
					gunAnim.Play ("ADS");
					isADS = true;
				}
			}


			if (isDown) {
				if (currentAmmo > 0) {
					AudioSource gunSound = GetComponent<AudioSource> ();
					if (isADS) {
						gunAnim.Play ("ADSFire");
					} else {
						gunAnim.Play ("Hipfire");
					}
					((PlayerShoot)FindObjectOfType (typeof(PlayerShoot))).Shoot ();
					muzzleFlash.Play ();
					gunSound.Play ();
					currentAmmo--;
				}


			} 

			if (Input.GetKeyDown (KeyCode.R)) {
				if (currentExtraAmmo > 0) {
					gunAnim.Play ("Reload");
					gunSound.PlayOneShot (assaultRifleReload);
					if (currentExtraAmmo >= maxClipSize) {
						currentExtraAmmo -= maxClipSize - currentAmmo;
						currentAmmo = maxClipSize;	
					} else {
						currentAmmo += currentExtraAmmo;
						currentExtraAmmo = 0;
					}
				}

			}

		}

	}
		
}
