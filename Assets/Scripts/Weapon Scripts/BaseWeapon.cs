﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
/*
 * standard public class that does not derive from mono 
 * since it does no come from mono - it needs to be serializable so 
 * unity knows how to save and use the class in the inspector
*/
[System.Serializable] 
public class BaseWeapon : MonoBehaviour  {

	public int gunID;

    public bool isFiring = false;
	public string type = "Handgun"; 
	public int damage = 10;
	public float maxRange = 20f; 
	public float fireRate = 0f; //0 is for single fire wepaon, higher than that it will fire many shots. 

	protected bool isADS = false;

	public int currentAmmo; 
	public int currentExtraAmmo;
	public int maxClipSize;
	public int maxExtraAmmo;

	//int internalAmmo;
	[SerializeField]
	public GameObject AmmoDisplay;

	//protected Text AmmoText;

	public Animation gunAnim;
	public AudioSource gunSound;

	public GameObject graphics;

	public ParticleSystem muzzleFlash;

	void Start(){

		gunAnim = GetComponent<Animation> ();
		gunSound = GetComponent<AudioSource> ();


		/*
		 * Debug.Log ("Trying ...");
		Text[]texts = AmmoDisplay.GetComponentsInChildren<Text>();
		Debug.Log ("Trying ... the arry is" + texts.Length);
		foreach (Text text in texts) {
			Debug.Log ("Trying to find that panel ");
			if (text.name == "ExtraAmmo") {
				Debug.Log ("Yup found it");
				AmmoText = text.GetComponent<Text>();
			}
		}

		}*/
	}

	public bool addAmmo (int ammount)
	{
		if (currentExtraAmmo < maxExtraAmmo) {
			currentExtraAmmo += ammount;
			if (currentExtraAmmo > maxExtraAmmo) {
				currentExtraAmmo = maxExtraAmmo;
			}
			return true; //means ammo was used 
		}
		return false; 
	}


	void Update () {
		//Debug.Log (AmmoText);
		//AmmoText.GetComponent<Text>().text = "" + currentAmmo + "/ " + currentExtraAmmo;

	}

	protected void UpdateAmmo(){
		AmmoDisplay = GameObject.FindGameObjectWithTag ("AmmoPanel");
		AmmoDisplay.GetComponent<AmmoPanelScript>().UpdateAmmoText(currentAmmo, currentExtraAmmo);
	}

	protected void UpdateAmmo(string text){
		AmmoDisplay = GameObject.FindGameObjectWithTag ("AmmoPanel");
		AmmoDisplay.GetComponent<AmmoPanelScript>().UpdateAmmoText(text);
	}

}

