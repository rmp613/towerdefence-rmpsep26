﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;

public class Tower : NetworkBehaviour {

    public const string TRIGUN = "Trigun";
    public const string RAILGUN = "Railgun";

    public string type;
	public GameObject prefab;
	public int price = 200;
	public List<Vector3> nodes = new List<Vector3>();
	private string _netID = "0";

	void Start(){
		if (gameObject != null)
			prefab = gameObject;
	}
	public Tower(string type, GameObject prefab, int price, Vector3 pos){
		this.type = type;
		this.prefab = prefab;
		this.price = price;
	}
	public Tower(GameObject prefab, int price){
		this.prefab = prefab;
		this.price = price;
	}
	public Tower(Tower tower){
		this.prefab = tower.prefab;
	}
	public Tower (GameObject prefab){
		this.prefab = prefab;
	}

	public bool SameType(Tower t2){
		if (type == t2.type) {
			return true;
		}
		return false;
	}
	public bool SameType(string t2){
		if (type == t2) {
			return true;
		}
		return false;
	}
}

