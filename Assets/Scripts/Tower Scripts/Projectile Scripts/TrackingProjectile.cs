﻿using UnityEngine;
using System.Collections;

public class TrackingProjectile : BaseProjectile {

    Vector3 mdirection;
    bool mfired;
    GameObject mlauncher;
    GameObject mtarget;
    public int mdamage;

    // Update is called once per frame
    void Update()
    {
        if (mtarget)
        {
            transform.position = Vector3.MoveTowards(transform.position, mtarget.transform.position, speed*Time.deltaTime);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public override void FireProjectile(GameObject launcher, GameObject target, int damage, float attackSpeed)
    {
        if (target)
        {
            mdirection = (target.transform.position - launcher.transform.position).normalized;
            mfired = true;
            mlauncher = launcher;
            mtarget = target;
            mdamage = damage;
        }
    }
	//TODO: figure out if this section should be a 	[Command]...
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
			other.gameObject.GetComponent<Enemy>().CmdTakeDamage(mdamage);
            Destroy(gameObject);
        }

    }
}
