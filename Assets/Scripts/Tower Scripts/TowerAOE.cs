﻿using UnityEngine;
using System.Collections;

public class TowerAOE : MonoBehaviour
{

    public float attackRate;
    public int damage;
    public float dps;
    float attacksPerSecond;

    public GameObject target;
    public float fieldofview;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        attacksPerSecond = 1 / attackRate;
        dps = (float)damage * attacksPerSecond;
        if (gameObject.GetComponent<TowerTracking>().trackCount > 0)
        {
            foreach (GameObject enemy in gameObject.GetComponent<TowerTracking>().trackingList)
            {
                if (gameObject.GetComponent<TowerTracking>().CheckVisibiity(enemy) == true)
                {
                    enemy.gameObject.GetComponent<Enemy>().CmdTakeDamage(damage);
                }
            }

        }
    }
}