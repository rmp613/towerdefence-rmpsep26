﻿using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections;
using System.Collections.Generic;

public class Enemy : BaseHealth {

    public const string PLACEHOLDER = "Placeholder";
    [SerializeField]
    public string type;
	[SerializeField]//offsetToUpdatePath is how far the current target is away from the end of the current path in order to get a new path
	protected float weaponDamage, weaponRange, moveSpeed, offsetToUpdatePath; 
	protected int currentPathIndex = 0;
	protected int pathAheadCheck = 5;
	protected int enemyId;

	protected List<Vector2> path;
	protected Vector3 moveTarget;
	protected Vector3 targetOldPos;

	protected Pathfinder pathfinder;
	protected Transform playerBase;
	protected Transform currentTarget;
    protected Vector3 targetPos;

    public GameObject explosion;
	public AudioSource hummingSource;
	public AudioClip hummingSound;
	public AudioClip hummingDamageSound;
	public AudioSource damageSource;
	public AudioClip[] ricochetSounds;
	public AudioClip explosionSound;

	public ParticleSystem sparks;
	public ParticleSystem damageSparks;

	public int fundsWorth;

	public bool isDead = false;

	public enum Target { PlayerBase, Player, Turret }
	private Target _target = Target.PlayerBase;
	public Target target {
		get { return _target; }
		set { _target = value; }
	}
    private float distanceToTarget;
	public void Start() {
		pathfinder = new Pathfinder();
		playerBase = GameObject.Find ("EndZone").transform;
		UpdatePath ();
		Physics.IgnoreCollision (GetComponent<BoxCollider> (), GameObject.FindGameObjectWithTag ("InWall").GetComponent<BoxCollider>());

		hummingSource.clip = hummingSound;
		hummingSource.Play ();
	}

	protected void Update () {
        if (GetComponent<ParticleSystem>()) {
            print("good)");
        }
        if(path.Count < 5 || Vector3.Distance(currentTarget.position, targetOldPos) > offsetToUpdatePath) {
            UpdatePath();
        }
        //if player moves or (add later if a tower is placed down) update path
        CmdDebug(transform.name + " has " + currentHealth);
		if (currentHealth <= maxHealth / 2) {
			if (hummingSource.clip.GetInstanceID () != hummingDamageSound.GetInstanceID ()) {
				hummingSource.Stop ();
				hummingSource.clip = hummingDamageSound;
				hummingSource.Play ();
			}

			if (!damageSparks.isPlaying) {
				damageSparks.Play ();
			}
		}
        distanceToTarget = Vector3.Distance(currentTarget.position, transform.position);
        //target closest. player or base
        UpdateTarget();
        FollowPath();
        targetOldPos = targetPos;
    }
    private void UpdateTarget() {
        Vector3 closestPlayer = GameManager.GetClosestPlayer(transform.position).transform.position;
        float distanceToPlayer = Vector3.Distance(closestPlayer, transform.position);
        float distanceToBase = Vector3.Distance(playerBase.position, transform.position);
        target = distanceToBase > distanceToPlayer ? Target.Player : Target.PlayerBase;
        targetPos = target == Target.Player ? closestPlayer : playerBase.position;
    }
    [Command]
    void CmdDebug(string s) {
        print(s);
    }
	void OnTriggerEnter(Collider collider){
        if(collider.transform.name == "EndZone") {
            Die();
            return;
        }
		UpdatePath ();
	}
    void OnCollisionEnter(Collision collision) {
        foreach(ContactPoint contact in collision.contacts)
        if (contact.otherCollider.transform.name == "EndZone") {
            Die();
            return;
        }else if (contact.otherCollider.transform.name.Contains("Player")) {
            Die();
            return;
         }
        UpdatePath();
    }
    protected override void Die (){
        Debug.Log("di)");
        Explode ();
    }
    [Command]
    public void CmdDie() {
        Debug.Log("Ded");
        Enemy enemy = default(Enemy);
        string ID = "";
        foreach (KeyValuePair<string, Enemy> kp in EnemyManager.enemies) {
            if (kp.Value.transform.position == transform.position) {
                enemy = kp.Value;
                ID = kp.Key;
                break;
            }
        }
        if (enemy != default(Enemy)) {
            Debug.Log(transform.name + " is Dead");
            EnemyManager.enemies.Remove(ID);
            NetworkServer.Destroy(enemy.gameObject);
        }
    }
	protected void Explode() {
        //damage playerbase if in range
		if (Vector3.Distance (playerBase.position, transform.position) < weaponRange) {
		//	if (!GetComponentInChildren<ParticleSystem> ().isPlaying) {
				playerBase.GetComponent<Base> ().CmdTakeDamage (weaponDamage);
                Debug.Log("Enemy hit base for: " + weaponDamage);
           // }
		}
        //damage players in range
		List<Player> playersInRange = GameManager.GetPlayersInRange (transform.position, weaponRange);
		if (playersInRange.Count > 0) {
			foreach (Player player in playersInRange) {
				player.CmdTakeDamage (weaponDamage);
				Debug.Log ("Enemy hit:" + player.transform.name+ " for " + weaponDamage + " left: " + player.currentHealth);
			}
		}

		GameObject.Instantiate (explosion, this.transform.position, new Quaternion());
		//IS DEAD is here
		isDead = true;
        CmdDie();
	}
	protected void FollowPath(){
		if(path != null && currentPathIndex < path.Count){
			if (!CheckPathClear ()) {//TODO: UPDATE THIS TO ONLY RUN SOMETIMES TO IMPROVE PERFORMANCE
				UpdatePath ();
			}
			if(moveTarget != transform.position){
				CalculateMoveTarget();
			}
			else{
				currentPathIndex++;
				CalculateMoveTarget();
			}
			float step = moveSpeed * Time.deltaTime;
			transform.position = Vector3.MoveTowards(transform.position, moveTarget, step);
		}
	}
	protected void CalculateMoveTarget(){
		moveTarget = Grid.GetWorldPos((int)path[currentPathIndex].x, (int)path[currentPathIndex].y);
		moveTarget = new Vector3(moveTarget.x, transform.position.y, moveTarget.z);
	}
	protected bool CheckPathClear(){
		for (int i = 0; currentPathIndex + i < path.Count && i < pathAheadCheck; i++) {
			if(!pathfinder.checkWalkable((int)path[currentPathIndex + i].x, (int)path[currentPathIndex + i].y)){
				return false;
			}
		}
		return true;
	}
	public void UpdatePath(){
		switch(target){
		case Target.Player:
			UpdatePath (GameManager.GetClosestPlayer(transform.position).gameObject.transform);
			break;
		case Target.PlayerBase:
			UpdatePath (playerBase);
			break;
		}
	}	
	protected void UpdatePath(Transform _newTarget){
		currentTarget = _newTarget;
        targetPos = _newTarget.position;
		try{
			path = pathfinder.findPath (Grid.GetVector2 (transform.position), Grid.GetVector2 (_newTarget.position));
		}catch(Exception e){
			Debug.LogError ("The error below is expected. Doesn't seem to affect anything and I decided to move on after trying to fix for a while. If you want to attemp to fix it, first delete this try catch as it moves the error. - Riordan");

		}
		currentPathIndex = 0;
	}
    [Command]
	public override void CmdTakeDamage(float _dmg){
		sparks.Play ();

		//For playing sounds only.
		int i = UnityEngine.Random.Range(0, ricochetSounds.Length);

		AudioClip clip = ricochetSounds [i];
		damageSource.Stop ();
		damageSource.clip = clip;
		damageSource.Play ();

        base.CmdTakeDamage(_dmg);
    }
}
