﻿using UnityEngine;
using System.Collections.Generic;

/*
 * Maily keeps track of the players - and also match settings
 * Since there is only one Game manger instance per scene, this has been made as a singleton
 * 
*/
public class GameManager : MonoBehaviour {

	public static GameManager singleton; 

	public MatchSettings matchsettings; 

	void Awake()
	{
		if (singleton != null) {
			Debug.LogError ("More than one Game manager is running"); 
		} else {
			singleton = this; 
			//singletons make it very easy to gain access to the Gamemanager without any gameobject.find or similar things. 
		}
	}

	#region GameStateController+UPDATEMETHOD

	public enum GameState { PHASE_BUILDING, PHASE_FIGHTING, PAUSED, WON, LOST };
	public static GameState state = GameState.PHASE_BUILDING;
	private bool firstPhase = true;
	[SerializeField]
	private float defaultNextPhaseCountdown = 5f;
	public static float nextPhaseCountdown = 0;
	public static float buildTime = 90f;

	void Update(){
		if (state == GameState.PHASE_BUILDING &&
		    Input.GetKeyDown (KeyCode.Return) &&
		    firstPhase) {
			nextPhaseCountdown = defaultNextPhaseCountdown;
			firstPhase = false;
		} else if (state == GameState.PHASE_BUILDING && !firstPhase) {
			//if enter key is pressed in normal build phase then speed up timer by setting it to 5 seconds
			if (nextPhaseCountdown > 5 && Input.GetKeyDown (KeyCode.Return)) {
				nextPhaseCountdown = 5;
			}
			//keep the phase countdown ticking down
			if (nextPhaseCountdown > 0) {
				nextPhaseCountdown -= Time.deltaTime;
			} else if (nextPhaseCountdown <= 0) { //if there is no time on the countdown transition into fighting phase
				state = GameState.PHASE_FIGHTING;
			}
		} else if (state == GameState.LOST) {
			;
		}

	}
	#endregion

	/// <summary>
	/// region are easy to keep track of sections of code - the first region here is for
	/// the player code
	/// </summary>
	#region player tracking 
	private const string PLAYER_ID_PREFIX = "Player "; 

	public static Dictionary<string, Player> players = new Dictionary<string, Player>();


	/*
	 * This is so each player adds the id from network idenity to its name so that
	 * it is displayed. RegisterPlayer can be used to create a dictionary of all the
	 * players in the session
	 * 
	*/
	public static bool IsPlayer(){
		if (players.Count > 0) {
			return true;
		}
		return false;
	}
	public static void RegisterPlayer(string _netID, Player _player)
	{
		string _playerID = PLAYER_ID_PREFIX + _netID; 
		players.Add (_playerID, _player);
		_player.transform.name = _playerID; //sets the name of the game object to the ID
	}

	public static void DeRegisterPlayer(string _playerID)
	{
		players.Remove (_playerID);
	}

	//you can get a player from this getter
	public static Player GetPlayer(string _playerID) {
        if(players.ContainsKey(_playerID))
		    return players[_playerID];
        print("Couldn't find enemy with ID: " + _playerID);
        return default(Player);
	}

	//Find the closest player to position
	public static Player GetClosestPlayer(Vector3 position){
		Player currentClosest = default(Player);
		float currentClosestDistance = 0f;
		foreach (Player player in players.Values) {
			float distance = Vector3.Distance (player.gameObject.transform.position, position);
			if(distance < currentClosestDistance || currentClosestDistance == 0f){
				currentClosest = player;
				currentClosestDistance = distance;
			}
		}
		if (currentClosest != default(Player)) {
			return currentClosest;
		}
		Debug.LogError ("No player found. Returned default player likely breaking script using this method");
		return default(Player);
	}
	public static List<Player> GetPlayersInRange(Vector3 position, float range){
		List<Player> list = new List<Player>();
		foreach (Player player in players.Values) {
			float distance = Vector3.Distance (player.gameObject.transform.position, position);
			if(distance <= range){
				list.Add (player);
			}
		}
		return list;
	}


	//if you want to visualise the dictionary of players it can be done from this script
	//I havent done it but if you want me to, I can work on it

	//This is just for testing - SHOULD BE COMMENTED OUT
	/*
	void OnGUI () note: found bug... the gun seems to be player 1. O.o
	{
	    GUILayout.BeginArea(new Rect(200, 200, 200, 500));
	    GUILayout.BeginVertical();

	    foreach (string _playerID in players.Keys)
	    {
	        GUILayout.Label(_playerID + "  -  " + players[_playerID].transform.name);
	    }

	    GUILayout.EndVertical();
	    GUILayout.EndArea();
	}
	*/ 

	#endregion



}
